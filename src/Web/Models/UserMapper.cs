﻿using System;
using Nancy;
using Nancy.Authentication.Forms;
using Nancy.Security;
using OneNoteNotebooks.Core;

namespace OneNoteNotebooks.Web.Models
{
    public class UserMapper : IUserMapper
    {
        private readonly IUserService _service;

        public UserMapper(IUserService service)
        {
            _service = service;
        }

        public IUserIdentity GetUserFromIdentifier(Guid identifier, NancyContext context)
        {
            var user = _service.GetUserFromIdentifier(identifier);

            return new UserIdentity(user);
        }
    }
}