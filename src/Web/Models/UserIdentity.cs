﻿using System.Collections.Generic;
using Nancy.Security;
using OneNoteNotebooks.Core;

namespace OneNoteNotebooks.Web.Models
{
    public class UserIdentity : IUserIdentity
    {
        public string UserName { get; private set; }
        public IEnumerable<string> Claims { get; private set; }

        public UserIdentity(User user)
        {
            UserName = user.UserName;
            Claims = new List<string>();
        }
    }
}