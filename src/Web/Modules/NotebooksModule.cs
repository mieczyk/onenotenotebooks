﻿using Nancy;
using Nancy.Security;
using OneNoteNotebooks.Core;
using OneNoteNotebooks.Core.OneNote;

namespace OneNoteNotebooks.Web.Modules
{
    public class NotebooksModule : NancyModule
    {
        public NotebooksModule(IUserService userService, IOneNoteClient oneNoteClient)
        {
            this.RequiresAuthentication();

            Get["/"] = _ =>
            {
                var user = userService.GetUserFromUserName(Context.CurrentUser.UserName);

                oneNoteClient.AccessToken = user.AccessToken;

                try
                {
                   var  notebooks = oneNoteClient.GetNotebooks();

                   return View["Index", notebooks];
                }
                catch (AccessTokenExpiredException)
                {
                    return Response.AsRedirect("~/account/login");
                }
            };
        }
    }
}