﻿using System;
using Nancy;
using Nancy.Authentication.Forms;
using OneNoteNotebooks.Core;
using OneNoteNotebooks.Core.OAuth2;

namespace OneNoteNotebooks.Web.Modules
{
    public class AccountModule : NancyModule
    {
        public AccountModule(IOAuth oauth, IUserService userService) : base("/account")
        {
            Get["/login"] = _ => Response.AsRedirect(oauth.BuildLoginUrl());

            Get["/logout"] = _ => Response.AsRedirect(oauth.BuildLogoutUrl());

            Get["/authorize"] = _ =>
            {
                var code = Request.Query["code"];
                var tokenInfo = oauth.GetToken(code) as TokenInfo;

                if (tokenInfo == null)
                {
                    return Response.AsRedirect("/account/login");
                }

                var user = userService.GetUserFromUserName(tokenInfo.UserId);
                var expiry = DateTime.Now.AddSeconds(tokenInfo.ExpiresIn);

                if (user == null)
                {
                    user = new User(tokenInfo);
                    userService.AddUser(user);
                }
                else
                {
                    userService.UpdateAccessToken(
                        user.Id,
                        tokenInfo.AccessToken, 
                        expiry
                    );
                }

                return this.LoginAndRedirect(user.Id, expiry);
            };

            Get["/clearup"] = _ =>
            {
                var user = userService.GetUserFromUserName(Context.CurrentUser.UserName);

                userService.UpdateAccessToken(user.Id, null, null);

                return this.LogoutAndRedirect("~/account/login");
            };
        }
    }
}