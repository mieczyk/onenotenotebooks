﻿using System.Configuration;
using Nancy;
using Nancy.Authentication.Forms;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
using OneNoteNotebooks.Core;
using OneNoteNotebooks.Core.OAuth2;
using OneNoteNotebooks.Web.Models;

namespace OneNoteNotebooks.Web.Infrastructure
{
    public class CustomBootstrapper : DefaultNancyBootstrapper
    {
        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            base.ConfigureApplicationContainer(container);

            container.Register<IOAuthConfig>((c, p) => new OAuthConfig(
                loginUrl: ConfigurationManager.AppSettings["OAuth.LoginUrl"],
                logoutUrl: ConfigurationManager.AppSettings["OAuth.LogoutUrl"],
                tokenUrl: ConfigurationManager.AppSettings["OAuth.TokenUrl"],
                loginRedirectUrl: ConfigurationManager.AppSettings["OAuth.LoginRedirectUrl"],
                logoutRedirectUrl: ConfigurationManager.AppSettings["OAuth.LogoutRedirectUrl"],
                clientId: ConfigurationManager.AppSettings["OAuth.ClientId"],
                clientSecret: ConfigurationManager.AppSettings["OAuth.ClientSecret"],
                scope: ConfigurationManager.AppSettings["OAuth.Scope"]
            ));

            container.Register<IDatabase>((c, p) => new Database(
                ConfigurationManager.ConnectionStrings["NotebooksDB"].ConnectionString
            ));

            container.Register<IUserMapper, UserMapper>();
        }

        protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            var formsAuthConfig = new FormsAuthenticationConfiguration
            {
                RedirectUrl = "~/account/login",
                UserMapper = container.Resolve<IUserMapper>()
            };

            FormsAuthentication.Enable(pipelines, formsAuthConfig);
        }
    }
}