﻿using System.Collections.Generic;
using Nancy.ViewEngines.Razor;

namespace OneNoteNotebooks.Web.Infrastructure
{
    public class RazorConfig : IRazorConfiguration
    {
        public IEnumerable<string> GetAssemblyNames()
        {
            yield return "OneNoteNotebooks.Core";
        }

        public IEnumerable<string> GetDefaultNamespaces()
        {
            yield return "OneNoteNotebooks.Core";
        }

        public bool AutoIncludeModelNamespace
        {
            get { return true; }
        }
    }
}