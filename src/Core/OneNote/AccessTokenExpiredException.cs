﻿using System;
using System.Runtime.Serialization;

namespace OneNoteNotebooks.Core.OneNote
{
    [Serializable]
    public class AccessTokenExpiredException : Exception
    {
        public AccessTokenExpiredException()
        {
        }

        public AccessTokenExpiredException(string message) : base(message)
        {
        }

        public AccessTokenExpiredException(string message, Exception inner) : base(message, inner)
        {
        }

        protected AccessTokenExpiredException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}