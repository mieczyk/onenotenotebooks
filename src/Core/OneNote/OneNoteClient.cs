﻿using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using RestSharp;

namespace OneNoteNotebooks.Core.OneNote
{
    public interface IOneNoteClient
    {
        string AccessToken { get; set; }

        IEnumerable<Notebook> GetNotebooks();
    }

    public class OneNoteClient : IOneNoteClient
    {
        private readonly IOneNoteClientConfig _config;
        private readonly RestClient _client;

        public string AccessToken { get; set; }

        public OneNoteClient(IOneNoteClientConfig config)
        {
            _config = config;
            _client = new RestClient(_config.BaseUrl);
        }

        public IEnumerable<Notebook> GetNotebooks()
        {
            var request = new RestRequest(_config.NotebooksUrl);

            var response = execute_request_with_access_token(request);

            var notebooks = JsonConvert.DeserializeObject<IEnumerable<Notebook>>(response.Value);

            return notebooks;
        }

        private OneNoteResponse execute_request_with_access_token(IRestRequest request)
        {
            check_access_token();

            request.AddHeader("Authorization", string.Format("Bearer {0}", AccessToken));

            var response = _client.Execute<OneNoteResponse>(request);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new AccessTokenExpiredException();
            }

            return response.Data;
        }

        private void check_access_token()
        {
            if (string.IsNullOrWhiteSpace(AccessToken))
            {
                throw new MissingAccessTokenException();
            }
        }
    }

    public class OneNoteResponse
    {
        public string Value { get; set; }
    }
}