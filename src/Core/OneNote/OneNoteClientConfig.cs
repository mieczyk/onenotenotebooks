﻿namespace OneNoteNotebooks.Core.OneNote
{
    public interface IOneNoteClientConfig
    {
        string BaseUrl { get; }
        string NotebooksUrl { get; }
    }

    public class OneNoteClientConfig : IOneNoteClientConfig
    {
        public string BaseUrl { get; private set; }
        public string NotebooksUrl { get; private set; }

        public OneNoteClientConfig()
        {
            BaseUrl = "https://www.onenote.com/api";
            NotebooksUrl = "/v1.0/me/notes/notebooks";
        }
    }
}