﻿using System;
using System.Runtime.Serialization;

namespace OneNoteNotebooks.Core.OneNote
{
    [Serializable]
    public class MissingAccessTokenException : Exception
    {
        public MissingAccessTokenException()
        {
        }

        public MissingAccessTokenException(string message) : base(message)
        {
        }

        public MissingAccessTokenException(string message, Exception inner) : base(message, inner)
        {
        }

        protected MissingAccessTokenException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}