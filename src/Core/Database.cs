﻿using System.Data;
using System.Data.SqlClient;

namespace OneNoteNotebooks.Core
{
    public interface IDatabase
    {
        IDbConnection OpenConnection();
    }

    public class Database : IDatabase
    {
        private readonly string _connectionString;

        public Database(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Opens database connection. Let the ADO.NET take care of connection pooling.
        /// </summary>
        /// <returns>Database connection</returns>
        public IDbConnection OpenConnection()
        {
            var connection = new SqlConnection(_connectionString);

            connection.Open();

            return connection;
        }
    }
}