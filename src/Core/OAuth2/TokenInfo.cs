﻿using RestSharp.Deserializers;

namespace OneNoteNotebooks.Core.OAuth2
{
    public class TokenInfo
    {
        [DeserializeAs(Name = "token_type")]
        public string TokenType { get; set; }

        [DeserializeAs(Name = "expires_in")]
        public int ExpiresIn { get; set; }

        public string Scope { get; set; }

        [DeserializeAs(Name = "access_token")]
        public string AccessToken { get; set; }

        [DeserializeAs(Name = "refresh_token")]
        public string RefreshToken { get; set; }

        [DeserializeAs(Name = "user_id")]
        public string UserId { get; set; }
    }
}