﻿using RestSharp;

namespace OneNoteNotebooks.Core.OAuth2
{
    public interface IOAuth
    {
        string BuildLoginUrl(string responseType = "code");
        string BuildLogoutUrl();
        TokenInfo GetToken(string code);
    }

    public class OAuth : IOAuth
    {
        private readonly IOAuthConfig _config;

        public OAuth(IOAuthConfig config)
        {
            _config = config;
        }

        public string BuildLoginUrl(string responseType = "code")
        {
            return string.Format(
                "{0}?response_type={1}&client_id={2}&redirect_uri={3}&scope={4}",
                _config.LoginUrl,
                responseType,
                _config.ClientId,
                _config.LoginRedirectUrl,
                _config.Scope
            ); 
        }

        public string BuildLogoutUrl()
        {
            return string.Format(
                "{0}?client_id={1}&redirect_uri={2}",
                _config.LogoutUrl,
                _config.ClientId,
                _config.LogoutRedirectUrl
            );
        }

        public TokenInfo GetToken(string code)
        {
            var client = new RestClient(_config.TokenUrl);

            var request = new RestRequest(Method.POST);

            request.AddParameter("grant_type", "authorization_code");
            request.AddParameter("client_id", _config.ClientId);
            request.AddParameter("client_secret", _config.ClientSecret);
            request.AddParameter("code", code);
            request.AddParameter("redirect_uri", _config.LoginRedirectUrl);

            var response = client.Execute<TokenInfo>(request);

            return response.Data;
        }
    }
}