﻿namespace OneNoteNotebooks.Core.OAuth2
{
    public interface IOAuthConfig
    {
        string LoginUrl { get; set; }
        string LogoutUrl { get; set; }
        string TokenUrl { get; set; }
        string LoginRedirectUrl { get; set; }
        string LogoutRedirectUrl { get; set; }
        string ClientId { get; set; }
        string ClientSecret { get; set; }
        string Scope { get; set; }
    }

    public class OAuthConfig : IOAuthConfig
    {
        public string LoginUrl { get; set; }
        public string LogoutUrl { get; set; }
        public string TokenUrl { get; set; }
        public string LoginRedirectUrl { get; set; }
        public string LogoutRedirectUrl { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Scope { get; set; }

        public OAuthConfig(
            string loginUrl
            ,string logoutUrl
            ,string tokenUrl
            ,string loginRedirectUrl
            ,string logoutRedirectUrl
            ,string clientId
            ,string clientSecret
            ,string scope
        )
        {
            LoginUrl = loginUrl;
            LogoutUrl = logoutUrl;
            TokenUrl = tokenUrl;
            LoginRedirectUrl = loginRedirectUrl;
            LogoutRedirectUrl = logoutRedirectUrl;
            ClientId = clientId;
            ClientSecret = clientSecret;
            Scope = scope;
        }
    }
}