﻿using System;
using OneNoteNotebooks.Core.OAuth2;

namespace OneNoteNotebooks.Core
{
    public class User
    {
        public Guid Id { get; private set; }
        public string UserName { get; set; }
        public string AccessToken { get; set; }
        public DateTime TokenExpirationTime { get; set; }

        public User()
        {
            Id = Guid.NewGuid();
        }

        public User(TokenInfo tokenInfo) : this()
        {
            UserName = tokenInfo.UserId;
            AccessToken = tokenInfo.AccessToken;
            TokenExpirationTime = DateTime.Now.AddSeconds(tokenInfo.ExpiresIn);
        }
    }
}