﻿using System;
using System.Linq;
using Dapper;

namespace OneNoteNotebooks.Core
{
    public interface IUserService
    {
        void AddUser(User user);
        void UpdateAccessToken(Guid userId, string accessToken, DateTime? tokenExpirationTime);
        User GetUserFromUserName(string userName);
        User GetUserFromIdentifier(Guid identifier);
    }

    public class UserService : IUserService
    {
        private readonly IDatabase _db;

        public UserService(IDatabase db)
        {
            _db = db;
        }

        public void AddUser(User user)
        {
            using (var connection = _db.OpenConnection())
            {
                connection.Execute(
                    @"INSERT INTO dbo.Users(Id, UserName, AccessToken, TokenExpirationTime)" +
                    @"VALUES(@Id, @UserName, @AccessToken, @TokenExpirationTime)",
                    user
                );
            }
        }

        public void UpdateAccessToken(Guid userId, string accessToken, DateTime? tokenExpirationTime)
        {
            using (var connection = _db.OpenConnection())
            {
                connection.Execute(
                    @"UPDATE dbo.Users " +
                    @"SET AccessToken = @AccessToken, TokenExpirationTime = @TokenExpirationTime " +
                    @"WHERE Id = @UserId",
                    new { AccessToken = accessToken, TokenExpirationTime = tokenExpirationTime, UserId = userId}
                );
            }
        }

        public User GetUserFromUserName(string userName)
        {
            using (var connection = _db.OpenConnection())
            {
               return connection.Query<User>(
                    @"SELECT * FROM dbo.Users WHERE UserName = @UserName", 
                    new { UserName = userName }
                ).FirstOrDefault();
            }
        }

        public User GetUserFromIdentifier(Guid identifier)
        {
            using (var connection = _db.OpenConnection())
            {
                return connection.Query<User>(
                     @"SELECT * FROM dbo.Users WHERE Id = @Id",
                     new { Id = identifier }
                 ).FirstOrDefault();
            }
        }
    }
}